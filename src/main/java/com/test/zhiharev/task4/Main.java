package com.test.zhiharev.task4;

public class Main {
    public static void main(String[] args) {
        //POINT 1
        String string = "I like Java!!!";
        System.out.println("Last char: " + string.charAt(string.length() - 1));
        System.out.println("String ends with '!!!': " + string.endsWith("!!!"));
        System.out.println("String starts with 'I like': " + string.startsWith("I like"));
        System.out.println("String contains 'Java': " + string.contains("Java"));
        System.out.println("Position of 'Java': " + string.indexOf("Java"));
        System.out.println("All 'a' chars are replaced by 'o': " + string.replace("a", "o"));
        System.out.println("String is in upper case: " + string.toUpperCase());
        System.out.println("String is in lower case: " + string.toLowerCase());
        String newString = string.substring(string.indexOf("Java"), string.indexOf("Java") + 4);
        System.out.println("New string: " + newString);

        //POINT 2
        // https://www.screencast.com/t/TL2yWkZ8BW
    }
}
