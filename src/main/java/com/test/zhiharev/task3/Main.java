package com.test.zhiharev.task3;

import com.test.zhiharev.task3.box.Box;
import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.Figure;
import com.test.zhiharev.task3.figures.GlassFigure;
import com.test.zhiharev.task3.figures.PaperFigure;
import com.test.zhiharev.task3.figures.circle.GlassCircle;
import com.test.zhiharev.task3.figures.circle.PaperCircle;
import com.test.zhiharev.task3.figures.square.GlassSquare;
import com.test.zhiharev.task3.figures.square.PaperSquare;
import com.test.zhiharev.task3.figures.triangle.GlassTriangle;
import com.test.zhiharev.task3.figures.triangle.PaperTriangle;

public class Main {
    public static void main(String[] args) {
        PaperFigure figure1 = new PaperTriangle(2, 3, 4, "blue");
        PaperFigure figure2 = new PaperSquare(5, "Red");
        PaperFigure figure3 = new PaperCircle(5, "Black");
        GlassFigure figure4 = new GlassTriangle(2, 3, 4, "blue");
        Figure figure5 = new GlassSquare(5, "TestColor");
        GlassFigure figure6 = new GlassCircle(5, "Black");

        System.out.println("Getting parameters of figures:");
        System.out.println("Square " + figure1.getSquare());
        System.out.println("Color " + figure2.getColor());
        System.out.println("Perimeter " + figure3.getPerimeter());
        System.out.println("Square " + figure4.getSquare());
        System.out.println("Color " + figure5.getColor());
        System.out.println("Perimeter " + figure6.getPerimeter());
        System.out.println();

        Box box = new Box();

        box.putFiguresInBox(figure1, figure2, figure3, figure4, figure5, figure6);
        System.out.println("All figures are put in box");
        box.getAllFigures();
        System.out.println();

        System.out.println("Elements in box by types");
        box.getAllFiguresByTypes();
        System.out.println();

        box.removeAllFiguresByType(FigureType.TRIANGLE);
        System.out.println("Triangles are removed");
        box.getAllFiguresByTypes();
        System.out.println();

        box.removeAllFiguresByType(FigureType.CIRCLE);
        System.out.println("Circles are removed");
        box.getAllFiguresByTypes();
        System.out.println();

        box.removeAllFiguresByType(FigureType.SQUARE);
        System.out.println("Squares are removed");
        box.getAllFiguresByTypes();
        System.out.println();

        box.putFiguresInBox(figure1, figure2, figure3, figure4, figure5, figure6);
        System.out.println("All figures are put in box");
        box.getAllFiguresByTypes();
        System.out.println();

        box.removeAllFigures();
        System.out.println("All figures was removed");
        box.getAllFiguresByTypes();
        System.out.println();

        box.putFiguresInBox(figure1);
        System.out.println("Figure 1 is put in box");
        System.out.println("Figure name is " + figure1.getFigureType());
        box.getAllFigures();
        System.out.println();

        figure1 = figure1.changeFigureToAnotherOne(box, FigureType.CIRCLE);
        System.out.println("Figure 1 is CIRCLE now");
        System.out.println("Figure name is " + figure1.getFigureType());
        box.getAllFigures();
        System.out.println();

        figure1 = figure1.changeFigureToAnotherOne(box, FigureType.TRIANGLE);
        System.out.println("Figure 1 is TRIANGLE now");
        System.out.println("Figure name is " + figure1.getFigureType());
        box.getAllFigures();
        System.out.println();

        figure1 = figure1.changeFigureToAnotherOne(box, FigureType.SQUARE);
        System.out.println("Figure 1 is SQUARE now");
        System.out.println("Figure name is " + figure1.getFigureType());
        box.getAllFigures();
        System.out.println();
    }
}
