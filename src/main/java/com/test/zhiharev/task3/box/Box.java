package com.test.zhiharev.task3.box;

import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.Figure;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Box {
    private Map<Figure, FigureType> box = new HashMap<Figure, FigureType>();

    public void putFiguresInBox(Figure... f) {
        for (Figure figure : f) {
            box.put(figure, figure.getFigureType());
        }
    }

    public void getAllFigures() {
        for (Map.Entry<Figure, FigureType> entry : box.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    public void getAllFiguresByTypes() {
        int n = 0;
        for (FigureType figure : FigureType.values()) {
            for (Map.Entry<Figure, FigureType> entry : box.entrySet()) {
                if (figure.equals(entry.getValue())) {
                    n++;
                }
            }
            System.out.println(figure.toString() + ": " + n);
            n = 0;
        }
    }

    public void removeAllFigures() {
        box.clear();
    }

    public void removeAllFiguresByType(FigureType figureType) {
        Iterator<Map.Entry<Figure, FigureType>> itr = box.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<Figure, FigureType> entry = itr.next();
            if (entry.getValue().equals(figureType)) {
                itr.remove();
            }
        }
    }

    public void removeFigure(Figure figureToRemove) {
        if (box.containsKey(figureToRemove)) {
            box.remove(figureToRemove);
        }
    }
}
