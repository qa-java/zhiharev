package com.test.zhiharev.task3.interfaces;

import com.test.zhiharev.task3.box.Box;
import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.PaperFigure;

public interface Paper {
    PaperFigure changeFigureToAnotherOne(Box box, FigureType newFigure);
}
