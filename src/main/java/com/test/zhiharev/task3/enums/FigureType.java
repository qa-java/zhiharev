package com.test.zhiharev.task3.enums;

public enum FigureType {
    TRIANGLE,
    SQUARE,
    CIRCLE
}
