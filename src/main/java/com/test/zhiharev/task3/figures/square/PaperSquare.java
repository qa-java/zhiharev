package com.test.zhiharev.task3.figures.square;

import com.test.zhiharev.task3.box.Box;
import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.PaperFigure;
import com.test.zhiharev.task3.figures.circle.PaperCircle;
import com.test.zhiharev.task3.figures.triangle.PaperTriangle;

public class PaperSquare extends PaperFigure {
    private double side;

    public PaperSquare(double side, String color) {
        this.side = side;
        this.color = color;
        this.figureType = FigureType.SQUARE;
    }

    public double getPerimeter() {
        return side * 4;
    }

    public double getSquare() {
        return side * side;
    }

    public PaperFigure changeFigureToAnotherOne(Box box, FigureType newFigure) {
        if (newFigure.equals(FigureType.TRIANGLE)) {
            box.removeFigure(this);
            double approximateSide = Math.sqrt(this.getSquare());
            PaperFigure createdFigure = new PaperTriangle(approximateSide, approximateSide, approximateSide, this.getColor());
            box.putFiguresInBox(createdFigure);
            return createdFigure;
        } else if (newFigure.equals(FigureType.CIRCLE)) {
            box.removeFigure(this);
            PaperFigure createdFigure = new PaperCircle(Math.sqrt(this.getSquare()) / 2, this.getColor());
            box.putFiguresInBox(createdFigure);
            return createdFigure;
        }
        return this;
    }
}
