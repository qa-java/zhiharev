package com.test.zhiharev.task3.figures;

import com.test.zhiharev.task3.enums.FigureType;

public abstract class Figure {
    protected String color;
    protected FigureType figureType;

    public abstract double getPerimeter();

    public abstract double getSquare();

    public String getColor() {
        return color;
    }

    public FigureType getFigureType() {
        return figureType;
    }
}
