package com.test.zhiharev.task3.figures.circle;

import com.test.zhiharev.task3.box.Box;
import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.PaperFigure;
import com.test.zhiharev.task3.figures.square.PaperSquare;
import com.test.zhiharev.task3.figures.triangle.PaperTriangle;

public class PaperCircle extends PaperFigure {
    private double radius;

    public PaperCircle(double radius, String color) {
        this.radius = radius;
        this.color = color;
        this.figureType = FigureType.CIRCLE;
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    public double getSquare() {
        return Math.PI * radius * radius;
    }


    public PaperFigure changeFigureToAnotherOne(Box box, FigureType newFigure) {
        if (newFigure.equals(FigureType.TRIANGLE)) {
            box.removeFigure(this);
            double approximateSide = Math.sqrt(this.getSquare() / Math.PI) * 3 / Math.sqrt(3);
            PaperFigure createdFigure = new PaperTriangle(approximateSide, approximateSide, approximateSide, this.getColor());
            box.putFiguresInBox(createdFigure);
            return createdFigure;
        } else if (newFigure.equals(FigureType.SQUARE)) {
            box.removeFigure(this);
            PaperFigure createdFigure = new PaperSquare(Math.sqrt(this.getSquare() / Math.PI * 2), this.getColor());
            box.putFiguresInBox(createdFigure);
            return createdFigure;
        }
        return this;
    }
}
