package com.test.zhiharev.task3.figures.square;

import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.GlassFigure;

public class GlassSquare extends GlassFigure {
    private double side;

    public GlassSquare(double side, String color) {
        this.side = side;
        this.color = color;
        this.figureType = FigureType.SQUARE;
    }

    public double getPerimeter() {
        return 4 * side;
    }

    public double getSquare() {
        return side * side;
    }
}
