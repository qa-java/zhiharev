package com.test.zhiharev.task3.figures.triangle;

import com.test.zhiharev.task3.box.Box;
import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.PaperFigure;
import com.test.zhiharev.task3.figures.circle.PaperCircle;
import com.test.zhiharev.task3.figures.square.PaperSquare;

public class PaperTriangle extends PaperFigure {
    private double side1;
    private double side2;
    private double side3;

    public PaperTriangle(double side1, double side2, double side3, String color) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.color = color;
        this.figureType = FigureType.TRIANGLE;
    }

    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    public double getSquare() {
        double halfPerimeter = getPerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - side1) * (halfPerimeter - side2) * (halfPerimeter - side3));
    }

    public PaperFigure changeFigureToAnotherOne(Box box, FigureType newFigure) {
        if (newFigure.equals(FigureType.SQUARE)) {
            box.removeFigure(this);
            PaperFigure createdFigure = new PaperSquare(Math.sqrt(this.getSquare()), this.getColor());
            box.putFiguresInBox(createdFigure);
            return createdFigure;
        } else if (newFigure.equals(FigureType.CIRCLE)) {
            box.removeFigure(this);
            PaperFigure createdFigure = new PaperCircle(Math.sqrt(this.getSquare() / Math.PI), this.getColor());
            box.putFiguresInBox(createdFigure);
            return createdFigure;
        }
        return this;
    }
}

