package com.test.zhiharev.task3.figures.triangle;

import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.GlassFigure;

public class GlassTriangle extends GlassFigure {
    private double side1;
    private double side2;
    private double side3;

    public GlassTriangle(double side1, double side2, double side3, String color) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.color = color;
        this.figureType = FigureType.TRIANGLE;
    }

    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    public double getSquare() {
        double halfPerimeter = getPerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - side1) * (halfPerimeter - side2) * (halfPerimeter - side3));
    }
}
