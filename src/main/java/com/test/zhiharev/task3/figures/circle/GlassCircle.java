package com.test.zhiharev.task3.figures.circle;

import com.test.zhiharev.task3.enums.FigureType;
import com.test.zhiharev.task3.figures.GlassFigure;

public class GlassCircle extends GlassFigure {
    private double radius;

    public GlassCircle(double radius, String color) {
        this.radius = radius;
        this.color = color;
        this.figureType = FigureType.CIRCLE;
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    public double getSquare() {
        return Math.PI * radius * radius;
    }

}
