package com.test.zhiharev.task5;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        //Point 1
        List<Integer> ints = new ArrayList<>();
        for (int i = 3; i < 21; i++) {
            ints.add(i);
        }
        long countOfEven = ints.stream()
                .filter(n -> n % 2 == 0)
                .count();
        System.out.println(countOfEven);

        //Point 2
        List<String> names = new ArrayList<>();
        names.add("Dima");
        names.add("Artem");
        names.add("Stas");
        names.add("Anton");
        names = names.stream()
                .filter(s -> s.charAt(0) == 'A')
                .collect(Collectors.toList());
        System.out.println(names);

        //Point 3
        List<String> strings = new ArrayList<>();
        strings.add("Dima");
        strings.add("Artem");
        strings.add("Stas");
        strings.add("Anton");
        strings = strings.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(strings);

        //Point 4
        List<Integer> ints2 = new ArrayList<>();
        for (int i = 21; i >= 3; i--) {
            ints2.add(i);
        }
        int secondMin = ints2.stream()
                .sorted()
                .skip(1)
                .findFirst()
                .orElse(0);
        System.out.println(secondMin);

        //Point 5
        List<String> strings3 = new ArrayList<>();
        strings3.add("Dima");
        strings3.add("Artem");
        strings3.add("Dima");
        strings3.add("Anton");
        strings3 = strings3.stream()
                .distinct()
                .collect(Collectors.toList());
        System.out.println(strings3);
    }
}
