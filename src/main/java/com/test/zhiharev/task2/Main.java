package com.test.zhiharev.task2;

public class Main {
    public static void main(String[] args) {
        point1(5, 10);
        point2(10, 4);
        point3(20);
        point4(2, 100);
        point5(2, 1, 2, 3, 4, 5, 2, 2, 1, 2, 5, 6, 7, 2);
    }

    public static void point1(int a, int b) {
        System.out.println("=============\nPOINT 1\n-------------");
        int c = a;
        System.out.println("a=" + a + ", b=" + b);
        a = b;
        b = c;
        System.out.println("a=" + a + ", b=" + b);
    }

    public static void point2(int iterations, int position) {
        System.out.println("=============\nPOINT 2\n-------------");
        for (int i = 0; i < iterations; i++) {
            if (i == position - 1) {
                System.out.println("This is the lucky apple!");
                break;
            }
        }
    }

    public static void point3(int arrayLength) {
        System.out.println("=============\nPOINT 3\n-------------");
        double max, min, sum = 0;
        double[] array = new double[arrayLength];
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.random() * 100;
        }
        min = max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) min = array[i];
            if (array[i] > max) max = array[i];
            sum += array[i];
        }
        System.out.println("max=" + max);
        System.out.println("min=" + min);
        System.out.println("avg=" + sum / arrayLength);
    }

    public static void point4(int rangeFrom, int rangeTo) {
        System.out.println("=============\nPOINT 4\n-------------");
        for (int i = rangeFrom; i <= rangeTo; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0 && j != 1 && j != i) {
                    break;
                }
                if (j == i) {
                    System.out.print(i + " ");
                }
            }
        }
    }

    public static void point5(int needsToRemove, int... originalArray) {
        System.out.println("\n=============\nPOINT 5\n-------------");
        int newArrayLength = 0;
        for (int i = 0; i < originalArray.length; i++) {
            if (originalArray[i] != needsToRemove) {
                newArrayLength++;
            }
        }
        int[] newIntArray = new int[newArrayLength];
        for (int i = 0, k = 0; i < originalArray.length; i++) {
            if (originalArray[i] != needsToRemove) {
                newIntArray[k] = originalArray[i];
                System.out.print(newIntArray[k] + " ");
                k++;
            }
        }
    }
}